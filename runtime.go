package hetaskapi

import (
	"github.com/casualjim/go-app"
	"github.com/casualjim/go-app/tracing"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"gitlab.com/Cryptle/hetaskapi/common"
	iface "gitlab.com/Cryptle/hetaskapi/interfaces"
)

// newRuntime creates a new application level runtime that encapsulates the shared services for this application
func NewRuntime(app app.Application) (*Runtime, error) {
	db, err := common.NewDBUserStore(app.Config())
	if err != nil {
		return nil, err
	}
	return &Runtime {
		db:  db,
		app: app,
	}, nil
}

// runtime encapsulates the shared services for this application
type Runtime struct {
	db  iface.Store
	app app.Application
}

// DB returns the persistent store
func (r *Runtime) DB() iface.Store {
	return r.db
}

// tracer returns the root tracer, this is typically the only one you need
func (r *Runtime) Tracer() tracing.Tracer {
	return r.app.Tracer()
}

// logger gets the root logger for this application
func (r *Runtime) Logger() logrus.FieldLogger {
	return r.app.Logger()
}

// newLogger creates a new named logger for this application
func (r *Runtime) NewLogger(name string, fields logrus.Fields) logrus.FieldLogger {
	return r.app.NewLogger(name, fields)
}

// config.json returns the viper config.json for this application
func (r *Runtime) Config() *viper.Viper {
	return r.app.Config()
}
