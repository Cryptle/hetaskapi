package common

import (
	. "gitlab.com/Cryptle/hetaskapi/gen/models"
	. "gitlab.com/Cryptle/hetaskapi/interfaces"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"github.com/spf13/viper"
	"log"
	"time"
)

const (
	COLLECTION = "users"
)

// create new users store backed by MongoDB
func NewDBUserStore(cfg *viper.Viper) (Store, error) {

	//get session after successful connection to DB
	session, err := mgo.DialWithInfo(&mgo.DialInfo{
		Addrs:    []string{cfg.GetString("db_host")},
		Username: cfg.GetString("db_username"),
		Password: cfg.GetString("db_password"),
		Timeout:  60 * time.Second,
	})

	if err != nil {
		log.Fatal(err)
		return nil, err
	}

	db := session.DB(cfg.GetString("database"))

	//set db object to class DB class member
	return &dbStore{DB: db}, nil
}

// database structure
type dbStore struct {
	DB *mgo.Database
}

// find list of users
func (dbs *dbStore) ListAll() ([]*User, error) {
	var users []*User
	err := dbs.DB.C(COLLECTION).Find(bson.M{}).All(&users)
	return users, err
}

// find a user by ID
func (dbs *dbStore) FindById(id string) (User, error) {
	var user User
	err := dbs.DB.C(COLLECTION).FindId(id).One(&user)
	return user, err
}

// insert a user into database
func (dbs *dbStore) Insert(user User) error {
	err := dbs.DB.C(COLLECTION).Insert(&user)
	return err
}

// delete an existing user
func (dbs *dbStore) Delete(id string) error {
	err := dbs.DB.C(COLLECTION).RemoveId(id)
	return err
}

// update an existing user
func (dbs *dbStore) Update(user User) error {
	err := dbs.DB.C(COLLECTION).UpdateId(user.ID, &user)
	return err
}
