package common

// new returns an error that formats as the given text.
func NewError(text string) error {
	return &errorString{text}
}

// errorString implementation of error.
type errorString struct {
	s string
}

func (e *errorString) Error() string {
	return e.s
}
