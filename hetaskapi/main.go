package main

import (
	theApp "github.com/casualjim/go-app"
	"github.com/go-openapi/loads"
	"gitlab.com/Cryptle/hetaskapi/gen/restapi/operations"
	"gitlab.com/Cryptle/hetaskapi/gen/restapi"
	"gitlab.com/Cryptle/hetaskapi/handlers"
	"github.com/sirupsen/logrus"
	"gitlab.com/Cryptle/hetaskapi"
	"github.com/justinas/alice"
	"github.com/casualjim/middlewares"
)

func main() {

	// setup new application with go-app
	app, err := theApp.New("hetaskapi")
	if err != nil {
		logrus.Fatalln(err)
	}

	// set loader
	log := app.Logger()

	config := app.Config()

	// set default db config.json value
	config.SetDefault("server", "localhost")
	config.SetDefault("port", 5000)

	// get runtime object we use it when we set the Handlers
	rt, err := hetaskapi.NewRuntime(app)
	if err != nil {
		log.Fatalln(err)
	}

	// load swagger spec
	swaggerSpec, err := loads.Analyzed(restapi.SwaggerJSON, "")
	if err != nil {
		log.Fatalln(err)
	}

	// create new service API
	api := operations.NewHetaskapiAPI(swaggerSpec)
	server := restapi.NewServer(api)
	defer server.Shutdown()

	// set all handlers
	api.UsersGetUsersHandler = handlers.NewListAllUsers(rt)
	api.UserUpdateUserHandler = handlers.NewUpdateUser(rt)
	api.UserAddUserHandler = handlers.NewAddUser(rt)
	api.UserUserByIDHandler = handlers.NewUserByID(rt)
	api.UserDeleteUserHandler = handlers.NewDeleteUser(rt)

	// configure middlewares with 'alice'
	handler := alice.New(
		middlewares.NewRecoveryMW(app.Info().Name, log),
		middlewares.NewAuditMW(app.Info(), log),
		middlewares.NewProfiler,
		middlewares.NewHealthChecksMW(app.Info().BasePath),
	).Then(api.Serve(nil))
	server.SetHandler(handler)

	// set server port from config file
	server.Port = config.GetInt("port")
	server.Host = config.GetString("server")

	// serve the api
	if err := server.Serve(); err != nil {
		log.Fatalln(err)
	}
}
