package tests

import (
	"testing"
	"strings"
	"net/http"
	"io/ioutil"
	"fmt"
)

func TestAddUser(t *testing.T) {

	addUserUrl := TEST_HOST+"/user/add"

	tests := []TestStruct{
		{`{}`, ERR_UnprocessableEntity, "", 0, ""},
		{`{"givenName":""}`, ERR_UnprocessableEntity, "", 0, ""},
		{`{"givenName":"july","email":"july@gm.com"}`, ERR_UnprocessableEntity, "", 0, ""},
		{`{"givenName":"july","email":"july@g.com","familyName" : "someName"}`, OK_Created, "", 0, ""},
	}

	for i, testCase := range tests {

		// string to reader
		reader := strings.NewReader(testCase.reqBody)
		request, err := http.NewRequest("POST", addUserUrl, reader)
		request.Header.Add("Content-Type", "application/json")

		res, err := http.DefaultClient.Do(request)
		if err != nil {
			// return error
			t.Error(err)
		}
		result, _ := ioutil.ReadAll(res.Body)

		tests[i].responseBody = strings.TrimSpace(string(result))
		tests[i].obsCode = res.StatusCode
	}

	DisplayResults("addNewUser", tests, t)
}

func TestUpdateUser(t *testing.T) {

	newUser, err := createNewUser()
	if err != nil {
		t.Error(err)
	}

	getUserUrl := fmt.Sprintf(TEST_HOST+"/user/%s", newUser.ID)
	getUserNotExistID := fmt.Sprintf(TEST_HOST+"/user/%s", "not_exist_id")

	tests := []TestStruct{
		{`{}`, ERR_UnprocessableEntity, "", 0, getUserUrl},
		{`{}`, ERR_UnprocessableEntity, "", 0, getUserNotExistID},
		{`{invalid_request}`, ERR_BadRequest, "", 0, getUserUrl},
		{`{"wrong":"request"}`, ERR_UnprocessableEntity, "", 0, getUserUrl},
		{`{"email": "test@test.com", "familyName": "test","givenName": "test"}`,
			OK_SuccessRequest, "", 0, getUserUrl},
	}

	for i, testCase := range tests {

		// string to reader
		reader := strings.NewReader(testCase.reqBody)
		request, err := http.NewRequest("PUT", testCase.url, reader)
		request.Header.Add("Content-Type", "application/json")

		res, err := http.DefaultClient.Do(request)
		if err != nil {
			// return error
			t.Error(err)
		}
		result, _ := ioutil.ReadAll(res.Body)

		tests[i].responseBody = strings.TrimSpace(string(result))
		tests[i].obsCode = res.StatusCode
	}

	DisplayResults("updateNewUser", tests, t)

	// delete the test user from db
	delErr := deleteUser(newUser.ID)
	if delErr != nil {
		t.Error(err)
	}
}

func TestDeleteUser(t *testing.T) {

	// create new user
	newUser, err := createNewUser()
	if err != nil {
		t.Error(err)
	}

	//construct url path with correct ID
	getUserUrl := fmt.Sprintf(TEST_HOST+"/user/%s", newUser.ID)
	getUserNotExistID := fmt.Sprintf(TEST_HOST+"/user/%s", "not_exist_id")

	tests := []TestStruct{
		{"", ERR_InternalServerError, "", 0, getUserNotExistID},
		{"", OK_NoContent, "", 0, getUserUrl},
	}

	for i, testCase := range tests {

		// string to reader
		reader := strings.NewReader(testCase.reqBody)
		request, err := http.NewRequest("DELETE", testCase.url, reader)
		request.Header.Add("Content-Type", "application/json")

		res, err := http.DefaultClient.Do(request)
		if err != nil {
			// return error
			t.Error(err)
		}
		result, _ := ioutil.ReadAll(res.Body)

		tests[i].responseBody = strings.TrimSpace(string(result))
		tests[i].obsCode = res.StatusCode
	}

	DisplayResults("deleteUser", tests, t)
}

func TestGetUser(t *testing.T) {

	newUser, err := createNewUser()
	if err != nil {
		t.Error(err)
	}

	//construct url path with correct ID
	getUserUrl := fmt.Sprintf(TEST_HOST+"/user/%s", newUser.ID)
	getUserNotExistID := fmt.Sprintf(TEST_HOST+"/user/%s", "not_exist_id")

	tests := []TestStruct{
		{"", ERR_InternalServerError, "", 0, getUserNotExistID},
		{"", OK_Created, "", 0, getUserUrl},
	}

	for i, testCase := range tests {

		// string to reader
		reader := strings.NewReader(testCase.reqBody)
		request, err := http.NewRequest("GET", testCase.url, reader)
		request.Header.Add("Content-Type", "application/json")

		res, err := http.DefaultClient.Do(request)
		if err != nil {
			// return error
			t.Error(err)
		}
		result, _ := ioutil.ReadAll(res.Body)

		tests[i].responseBody = strings.TrimSpace(string(result))
		tests[i].obsCode = res.StatusCode
	}

	DisplayResults("getUser", tests, t)

	// delete the test user from db
	delErr := deleteUser(newUser.ID)
	if delErr != nil {
		t.Error(err)
	}
}
