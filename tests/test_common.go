package tests

import (
	"testing"
	"strings"
	"net/http"
	"io/ioutil"
	"encoding/json"
	"gitlab.com/Cryptle/hetaskapi/gen/models"
	"fmt"
)

// constants with response codes
const (
	OK_SuccessRequest       = 200
	OK_Created              = 201
	OK_NoContent            = 204
	ERR_BadRequest          = 400
	ERR_UnprocessableEntity = 422
	ERR_InternalServerError = 500
	TEST_HOST				= "http://localhost:5000"
)

// the test structure
type TestStruct struct {
	reqBody      string
	expectCode   int
	responseBody string
	obsCode      int
	url          string
}

// test and display the test results
func DisplayResults(funName string, tests []TestStruct, t *testing.T) {

	for _, test := range tests {

		if test.obsCode == test.expectCode {
			t.Logf("Passed Case:\n request body : %s \n expectedStatus : %d \n responseBody : %s \n "+
				"observedStatusCode : %d \n Url : %s\n",
				test.reqBody,
				test.expectCode,
				test.responseBody,
				test.obsCode,
				test.url)
		} else {
			t.Errorf("Failed Case:\n request body : %s \n expectedStatus : %d \n responseBody : %s \n "+
				"observedStatusCode : %d \n Url : %s\n",
				test.reqBody,
				test.expectCode,
				test.responseBody,
				test.obsCode,
				test.url)
		}
	}
}

func createNewUser() (*models.User, error) {

	//create user and get ID
	addUserUrl := TEST_HOST+"/user/add"

	// construct the request with some dummy data for the user
	reader := strings.NewReader(`{"givenName":"test","email":"test@test.com","familyName" : "test"}`)
	request, err := http.NewRequest("POST", addUserUrl, reader)
	request.Header.Add("Content-Type", "application/json")

	// send post request to the api and get user
	res, err := http.DefaultClient.Do(request)
	if err != nil {
		return nil, err
	}

	// get the result of any
	result, _ := ioutil.ReadAll(res.Body)
	usrJson := strings.TrimSpace(string(result))

	// unmarshal json to user object
	newUser := models.User{}
	if err := json.Unmarshal([]byte(usrJson), &newUser); err != nil {
		return nil, err
	}

	return &newUser, nil
}

func deleteUser(id string) error {

	//delete user url
	delUserUrl := fmt.Sprintf(TEST_HOST+"/user/%s", id)

	// construct the request with empty body
	reader := strings.NewReader("")
	request, err := http.NewRequest("DELETE", delUserUrl, reader)
	request.Header.Add("Content-Type", "application/json")

	// send delete request to the api
	_, err = http.DefaultClient.Do(request)
	if err != nil {
		return err
	}

	return nil
}
