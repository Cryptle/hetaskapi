package handlers

import (
	"github.com/go-openapi/runtime/middleware"
	"gitlab.com/Cryptle/hetaskapi"
	"gitlab.com/Cryptle/hetaskapi/gen/restapi/operations/user"
	"github.com/go-openapi/swag"
	"gitlab.com/Cryptle/hetaskapi/gen/models"
	"net/http"
	"github.com/go-openapi/strfmt"
	"gopkg.in/mgo.v2/bson"
	"time"
	"gitlab.com/Cryptle/hetaskapi/common"
)

// add user handler
func NewAddUser(rt *hetaskapi.Runtime) user.AddUserHandler {
	return &addUser{rt: rt}
}

// add user struct
type addUser struct {
	rt *hetaskapi.Runtime
}

// handle add new user
func (la *addUser) Handle(params user.AddUserParams) middleware.Responder {

	if params.Body == nil || params.Body.Email == nil || params.Body.FamilyName == nil || params.Body.GivenName == nil {
		cerr := common.NewError("Some of the required fields are empty")
		return user.NewUpdateUserDefault(http.StatusInternalServerError).WithPayload(modelsError(cerr))
	}

	email := swag.StringValue(params.Body.Email)
	fName := swag.StringValue(params.Body.FamilyName)
	gName := swag.StringValue(params.Body.GivenName)

	// current time
	timestamp := strfmt.DateTime(time.Now()).String()

	// generate new ObjectId
	id := strfmt.ObjectId(bson.NewObjectId().Hex())

	usr := models.User{ID: id.String(), Email: &email, FamilyName: &fName, GivenName: &gName, Created: timestamp}

	err := la.rt.DB().Insert(usr)
	if err != nil {
		return user.NewAddUserDefault(http.StatusInternalServerError).WithPayload(modelsError(err))
	}
	return user.NewAddUserCreated().WithPayload(&usr)
}
