package handlers

import (
	"github.com/go-openapi/runtime/middleware"
	"gitlab.com/Cryptle/hetaskapi"
	"gitlab.com/Cryptle/hetaskapi/gen/restapi/operations/user"
	"net/http"
	"github.com/go-openapi/swag"
	"gitlab.com/Cryptle/hetaskapi/common"
)

// new userByID handler
func NewUserByID(rt *hetaskapi.Runtime) user.UserByIDHandler {
	return &getUserByID{rt: rt}
}

// userByID private class
type getUserByID struct {
	rt *hetaskapi.Runtime
}

// get user by ID
func (la *getUserByID) Handle(params user.UserByIDParams) middleware.Responder {

	if params.ID == "" {
		cerr := common.NewError("ID is empty")
		return user.NewUpdateUserDefault(http.StatusInternalServerError).WithPayload(modelsError(cerr))
	}

	id := swag.StringValue(&params.ID)
	usr, err := la.rt.DB().FindById(id)
	if err != nil {
		return user.NewUserByIDDefault(http.StatusInternalServerError).WithPayload(modelsError(err))
	}
	return user.NewUserByIDCreated().WithPayload(&usr)
}
