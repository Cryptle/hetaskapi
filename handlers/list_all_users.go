package handlers

import (
	"github.com/go-openapi/swag"
	"github.com/go-openapi/runtime/middleware"
	"net/http"
	"gitlab.com/Cryptle/hetaskapi/gen/restapi/operations/users"
	"gitlab.com/Cryptle/hetaskapi/gen/models"
	"gitlab.com/Cryptle/hetaskapi"
)

func modelsError(err error) *models.Error {
	return &models.Error{
		Message: swag.String(err.Error()),
	}
}

// new ListAll handler
func NewListAllUsers(rt *hetaskapi.Runtime) users.GetUsersHandler {
	return &listAllUsers{rt: rt}
}

// the ListAll private class
type listAllUsers struct {
	rt *hetaskapi.Runtime
}

// handle for ListAll
func (la *listAllUsers) Handle(params users.GetUsersParams) middleware.Responder {

	dbUsers, err := la.rt.DB().ListAll()
	if err != nil {
		return users.NewGetUsersDefault(http.StatusInternalServerError).WithPayload(modelsError(err))
	}
	return users.NewGetUsersOK().WithPayload(dbUsers)
}
