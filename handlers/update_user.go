package handlers

import (
	"github.com/go-openapi/runtime/middleware"
	"gitlab.com/Cryptle/hetaskapi"
	"gitlab.com/Cryptle/hetaskapi/gen/restapi/operations/user"
	"github.com/go-openapi/swag"
	"gitlab.com/Cryptle/hetaskapi/gen/models"
	"net/http"
	"gitlab.com/Cryptle/hetaskapi/common"
)

// new update handler
func NewUpdateUser(rt *hetaskapi.Runtime) user.UpdateUserHandler {
	return &updateUser{rt: rt}
}

// update user private class
type updateUser struct {
	rt *hetaskapi.Runtime
}

// update user handle
func (la *updateUser) Handle(params user.UpdateUserParams) middleware.Responder {

	if params.ID == "" || params.Body == nil || params.Body.Email == nil || params.Body.FamilyName == nil || params.Body.GivenName == nil {
		cerr := common.NewError("Some of the required fields are empty")
		return user.NewUpdateUserDefault(http.StatusInternalServerError).WithPayload(modelsError(cerr))
	}

	id := swag.StringValue(&params.ID)
	email := swag.StringValue(params.Body.Email)
	fName := swag.StringValue(params.Body.FamilyName)
	gName := swag.StringValue(params.Body.GivenName)

	usr := models.User{ID: id, Email: &email, FamilyName: &fName, GivenName: &gName}

	err := la.rt.DB().Update(usr)
	if err != nil {
		return user.NewUpdateUserDefault(http.StatusInternalServerError).WithPayload(modelsError(err))
	}
	return user.NewUpdateUserOK()
}
