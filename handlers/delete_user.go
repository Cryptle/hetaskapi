package handlers

import (
	"github.com/go-openapi/runtime/middleware"
	"gitlab.com/Cryptle/hetaskapi"
	"gitlab.com/Cryptle/hetaskapi/gen/restapi/operations/user"
	"net/http"
	"github.com/go-openapi/swag"
	"gitlab.com/Cryptle/hetaskapi/common"
)

// new deleteUser handler
func NewDeleteUser(rt *hetaskapi.Runtime) user.DeleteUserHandler {
	return &deleteUser{rt: rt}
}

// deleteUser private class
type deleteUser struct {
	rt *hetaskapi.Runtime
}

// deleteUser handler
func (la *deleteUser) Handle(params user.DeleteUserParams) middleware.Responder {

	if params.ID == "" {
		cerr := common.NewError("ID is empty")
		return user.NewUpdateUserDefault(http.StatusInternalServerError).WithPayload(modelsError(cerr))
	}

	err := la.rt.DB().Delete(swag.StringValue(&params.ID))
	if err != nil {
		return user.NewDeleteUserDefault(http.StatusInternalServerError).WithPayload(modelsError(err))
	}
	return user.NewDeleteUserNoContent()
}
