# golang image where workspace (GOPATH) configured at /go.
FROM golang:1.11.2

# Install dependencies
RUN go get gopkg.in/mgo.v2

# copy the local package files to the container workspace
ADD . /go/src/gitlab.com/Cryptle/hetaskapi

# setting up working directory
WORKDIR /go/src/gitlab.com/Cryptle/hetaskapi

# build the hetaskapi command inside the container.
RUN go install gitlab.com/Cryptle/hetaskapi/hetaskapi

# Run the hetaskapi service when the container starts.
ENTRYPOINT /go/bin/hetaskapi

# Service listens on port 5000.
EXPOSE 5000
