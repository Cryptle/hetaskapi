package interfaces

import . "gitlab.com/Cryptle/hetaskapi/gen/models"

// Store interface
type Store interface {
	ListAll() ([]*User, error)
	FindById(id string) (User, error)
	Insert(user User) error
	Delete(id string) error
	Update(user User) error
}
